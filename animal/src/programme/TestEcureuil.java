package programme;
import static org.junit.Assert.*;
import org.junit.Test;


public class TestEcureuil {
	
	@Test
	public void TestConstructeurEcureuil1() {
		Ecureuil e = new Ecureuil();
		assertEquals("Ecureuil devrais avoir 5 Pv",e.getPv(),5);
		assertEquals("Ecureuil devrais avoir 0 noisettes",e.getStockNourriture(),0);
	}
	
	@Test
	public void TestConstreurParamPvPos() {
		Ecureuil e = new Ecureuil(10);
		assertEquals("Ecureuil devrais avoir 10 Pv",e.getPv(),10);
	}
	
	@Test
	public void TestConstreurParamPvNul() {
		Ecureuil e = new Ecureuil(0);
		assertEquals("Ecureuil devrais avoir 0 Pv",e.getPv(),0);
		assertEquals("Ecureuil devrais etre mort",e.etreMort(),true);
	}
	
	@Test
	public void TestConstreurParamPvNeg() {
		Ecureuil e = new Ecureuil(-10);
		assertEquals("Ecureuil devrais avoir 5 Pv",e.getPv(),5);
	}
	
	@Test
	public void TestStockerNourriturePos() {
		Ecureuil e = new Ecureuil();
		e.stockerNourriture(2);
		assertEquals("Ecureuil devrais avoir 2 noisettes",e.getStockNourriture(),2);
		}
	
	@Test
	public void TestStockerNourritureNul() {
		Ecureuil e = new Ecureuil();
		e.stockerNourriture(5);
		e.stockerNourriture(0);
		assertEquals("Ecureuil devrais avoir 5 noisettes",e.getStockNourriture(),5);
		}

	
	@Test
	public void TestStockerNourritureNeg() {
		Ecureuil e = new Ecureuil();
		e.stockerNourriture(5);
		e.stockerNourriture(-5);
		assertEquals("Ecureuil devrais avoir 5 noisettes",e.getStockNourriture(),5);
		}
}
