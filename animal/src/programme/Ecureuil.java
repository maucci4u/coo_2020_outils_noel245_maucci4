package programme;

public class Ecureuil implements Animal {
	private int noisette;
	private int pointDeVie;
	
	public Ecureuil() {
		this.noisette = 0;
		this.pointDeVie = 5;
	}
	
	public Ecureuil(int pdv) {
		if (pdv >= 0){
			this.pointDeVie = pdv;
		} else {
			this.pointDeVie = 5;
		}
		this.noisette = 0;
	}
	
	public void stockerNourriture(int nourriture) {
		if (nourriture >= 0){
			this.noisette += nourriture;
		}
	}
	
	public boolean etreMort() {
		return (this.pointDeVie <= 0);
	}
	
	public boolean passerUnjour() {
		boolean res;
		if(this.etreMort()) {
			res = false;
		} 
		if(this.noisette >= 1) {
			this.noisette -= 1;
		} else {
			this.pointDeVie -= 2;
		}
		if(this.etreMort()) {
			res = false;
		} else {
			res = true;
		}
		return res;
		
	}
	
	public int getPv() {
		return this.pointDeVie;
	}
	
	public int getStockNourriture() {
		return this.noisette;
	}
	
	
	
	public String toString() {
		String s = "Ecureuil - pv: " + this.getPv() + " noisettes: " + this.getStockNourriture(); 
		return s;
	}
	
}

