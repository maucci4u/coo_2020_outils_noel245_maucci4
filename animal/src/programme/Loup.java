package programme;

public class Loup implements Animal {
	
	private int PV;
	private int viande;
	
	public Loup() {
		this.PV = 30;
		this.viande = 0;
	}
	
	public Loup(int HP) {
		if (HP >= 0) {
			this.PV = HP;
		} else {
		this.PV = 30;
		}
		this.viande = 0;
	}
	
	
	public boolean etreMort() {
		return this.getPv() <= 0;
	}

	/**
	 * fait evoluer l'animal d'un jour l'oblige a se nourrir ou a perdre des
	 * points de vie
	 * 
	 * @return true si l'animal est vivant a la fin du jour
	 */
	public boolean passerUnjour() {
		boolean res = true;
		if (this.etreMort()) {
			res = false;
		} else {
			if (this.getStockNourriture() <= 0) {
				this.PV= this.PV-4;
				if (this.etreMort()) {
					res = false;
				}
			} else {
				this.viande = this.viande-1;
			}
		}
		this.viande=this.viande/2;
		return res;
	}

	/**
	 * permet d'ajouter de la nourriture a son stock
	 * 
	 * @param nourriture
	 *            quantite de nourriture stockee
	 */
	public void stockerNourriture(int nourriture) {
		if (nourriture >= 0) {
		this.viande = this.viande + nourriture;
		}
	}

	/**
	 * retourne les points de vie de l'animal
	 * @return points de vie de l'animal
	 */
	public int getPv() {
		return this.PV;
	}
	
	/**
	 * retourne le stocke de nourriture possede par l'animal
	 * @return nourriture de l'animal
	 */
	public int getStockNourriture() {
		return this.viande;
	}
	
	public String toString() {
		return "Loup - pv:" + this.getPv() + " viande:" + this.getStockNourriture();
	}
}
