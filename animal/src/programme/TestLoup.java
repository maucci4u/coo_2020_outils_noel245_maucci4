package programme;
import static org.junit.Assert.*;
import org.junit.Test;


public class TestLoup {
	
	@Test
	public void TestConstructeurVideLoup() {
		Loup l = new Loup();
		assertEquals("Loup devrais avoir 30 Pv",l.getPv(),30);
		assertEquals("Loup devrais avoir 0 viande",e.getStockNourriture(),0);
	}
	
	@Test
	public void TestConstructeurParamPvPos() {
		Loup l = new Loup(10);
		assertEquals("Loup devrais avoir 10 Pv",l.getPv(),10);
	}
	
	@Test
	public void TestConstructeurParamPvNul() {
		Loup l = new Loup(0);
		assertEquals("Loup devrais avoir 0 Pv",l.getPv(),0);
		assertEquals("Loup devrais etre mort",l.etreMort(),true);
	}
	
	@Test
	public void TestConstructeurParamPvNeg() {
		Loup l = new Loup(-10);
		assertEquals("Loup devrais avoir 30 Pv",l.getPv(),30);
	}
	
	@Test
	public void TestStockerNourriturePos() {
		Loup l = new Loup();
		l.stockerNourriture(5);
		assertEquals("Loup devrais avoir 2 viandes",l.getStockNourriture(),5);
		}
	
	@Test
	public void TestStockerNourritureNul() {
		Loup l = new Loup();
		l.stockerNourriture(7);
		l.stockerNourriture(0);
		assertEquals("Loup devrais avoir 7 viandes",e.getStockNourriture(),7);
		}

	
	@Test
	public void TestStockerNourritureNeg() {
		Loup l = new Loup();
		l.stockerNourriture(7);
		l.stockerNourriture(-5);
		assertEquals("Loup devrais avoir 7 viandes",l.getStockNourriture(),7);
		}

}
